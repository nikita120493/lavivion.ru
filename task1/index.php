<!DOCTYPE html>
<html>
<head>
    <title>Задача 1</title>
    <meta charset="utf-8">
</head>
<body>

<form method="POST" enctype="multipart/form-data">
    <p>
        <label for="name">Видите имя:</label>

        <input type="text" name="name" required>
    </p>
    <p>
        <label for="surname">Видите Фамилия:</label>

        <input type="text" name="surname" required>
    </p>
    <p>
        <label for="avatar">Загрузить аватарку:</label>

        <input type="file" name="avatar">
    </p>
    <p>
        <input type="submit" value="Отправить">
        <input type="reset" value="Сброc">
    </p>
</form>

<?php

if (isset($_POST['name']) && isset($_POST['surname'])) {
    $name = $_POST['name'];
    $surname = $_POST['surname'];

    if (empty($_FILES['avatar']['name'])) {
        echo "$name $surname <br /> Аватарка не загружена";
        return;
    }

    $avatarError = $_FILES['avatar']['error'];

    if ($avatarError !== 0) {
        echo "Ошибка загрузки аватара.";
        return;
    }

    if (!file_exists('avatars')) {
        mkdir('avatars');
    }

    $avatarName = $_FILES['avatar']['name'];
    $extension = pathinfo($avatarName, PATHINFO_EXTENSION);

    $avatarTmp = $_FILES['avatar']['tmp_name'];
    $avatarSize = $_FILES['avatar']['size'];
    $avatarType = $_FILES['avatar']['type'];

    $avatarPath =
        sprintf(
            "avatars/%s.%s",
            md5($avatarName . rand(1, 100)),
            $extension
        );

    $imageFullPath =
        sprintf(
            'http://%s/%s/%s',
            $_SERVER['HTTP_HOST'],
            basename(__DIR__),
            $avatarPath
        );

    if ($avatarError == 0) {
        move_uploaded_file($avatarTmp, $avatarPath);
        echo "<img src='task1/$avatarPath' alt='Аватар'>";
//            echo "<img src='$imagePath' alt='Аватар'>";
    }
}

?>
</body>
</html>
