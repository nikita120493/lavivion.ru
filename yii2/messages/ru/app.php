<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Active' => 'Активный',
    'Are you sure you want to delete this item?' => 'Вы уверены, что хотите удалить этот элемент?',
    'Author ID' => 'Автор',
    'Authors' => 'Авторы',
    'Books' => 'Книги',
    'Create Author' => 'Создать автора',
    'Create Book' => 'Создать книгу',
    'Created At' => 'Создан',
    'Created By' => 'Сделано',
    'Delete' => 'Удалить',
    'Description' => 'Описание',
    'English' => 'Английский',
    'Inactive' => 'Неактивный',
    'Is Status' => 'Статус',
    'Name' => 'Имя',
    'Reset' => 'Перезагрузить',
    'Russian' => 'Русский',
    'Save' => 'Сохранять',
    'Search' => 'Поиск',
    'Seo Description' => 'SEO-описание',
    'Seo Keywords' => 'SEO-ключевые слова',
    'Seo Title' => 'Сео Название',
    'Short Description' => 'Краткое описание',
    'The requested page does not exist.' => 'Запрашиваемая страница не существует.',
    'Title' => 'Заголовок',
    'Update' => 'Обновлять',
    'Update Author: {name}' => 'Автор обновления: {name}',
    'Update Book: {name}' => 'Обновление книги: {name}',
    'Updated At' => 'Обновлено в',
    'Updated By' => 'Обновлено',
    'Administration' => 'Администрирование',
    'Assignment' => 'Назначение',
    'Create User' => 'Создать пользователя',
    'ID' => '',
    'Permission' => 'Разрешение',
    'Role' => 'Роль',
    'Routes' => 'Маршруты',
    'Slug' => '',
    'Update User: {name}' => 'Обновить пользователя: {name}',
    'User' => 'Пользователь',
    'Username' => 'Имя пользователя',
    'Users' => 'Пользователи',
    'Quantity of books' => 'Количество книг',

];
