<?php

/** @var yii\web\View $this */

$this->title = Yii::t('app','Authors');

use yii\helpers\Html;

use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'format'=>'html',
                'value' => function ($model) {
                    return Html::a($model->name, ['authors/books', 'slug' => $model->slug]);
                }
            ],
            'short_description',
            'created_at',
        ],
    ]); ?>

</div>
