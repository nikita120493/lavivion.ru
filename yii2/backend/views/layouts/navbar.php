<?php

use yii\helpers\Html;

$url = sprintf('/%s/%s', Yii::$app->controller->id, Yii::$app->controller->action->id);
$getRequest = \Yii::$app->request->get();

$languages = [
    'ru' => Yii::t('app', 'Russian'),
    'en' => Yii::t('app', 'English'),
]

?>
<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <?= $languages[Yii::$app->language] ?>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                <?php

                foreach ($languages as $alias => $language) {
                ?>

                    <a class="dropdown-item"
                       href="<?= \yii\helpers\Url::to([$url, 'language' => $alias, $getRequest]) ?>"><?= $language ?>
                    </a>
                    <div class="dropdown-divider"></div>
                <?php
                }?>
            </div>
        </li>

        <li class="nav-item">
            <?= Html::a('<i class="fas fa-sign-out-alt"></i>', ['/site/logout'], ['data-method' => 'post', 'class' => 'nav-link']) ?>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                <i class="fas fa-expand-arrows-alt"></i>
            </a>
        </li>
    </ul>
</nav>
