<?php

use yii\db\Migration;

/**
 * Class m230604_172046_added_user
 */
class m230604_172046_added_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user = new \common\models\User();
        $user->username = 'admin';
        $user->email = 'admin@example.com';
        $user->status = 10;
        $user->generateAuthKey();
        $user->setPassword('admin1234');
        $user->save(false);

    }

}
