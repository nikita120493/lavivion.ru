<?php

namespace backend\controllers;

use backend\models\UserForm;
use common\models\User;
use backend\models\UserSearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


class UsersController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'actions' => ['create',],
                            'allow' => true,
                            'roles' => ['createUser']
                        ],
                        [
                            'actions' => [ 'update'],
                            'allow' => true,
                            'roles' => ['updateUser']
                        ],
                        [
                            'actions' => [ 'delete'],
                            'allow' => true,
                            'roles' => ['deleteUser']
                        ],
                        [
                            'actions' => ['index', 'view'],
                            'allow' => true,
                            'roles' => ['manger', 'administration', 'admin'],
                        ],
                    ],
                ],
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all User models.
     *
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param int $id
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return string | \yii\web\Response
     * @throws \yii\base\Exception
     */
    public function actionCreate(): string
    {
        $model = new UserForm();
        $model->scenario = 'create';

        if ($this->request->isPost && $model->load($this->request->post())) {
            if ($user = $model->saveData()) {
                return $this->redirect(['view', 'id' => $user->id]);
            }
        }

        return $this->render('create', compact('model'));
    }


    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUpdate($id): string
    {
        $user = $this->findModel($id);
        $model = new UserForm([
            'email' => $user->email,
            'username' => $user->username,
            'status' => $user->status,
        ]);

        if ($model->load(Yii::$app->request->post())) {
            $user->status = $model->status;
            $user->email = $model->email;
            $user->username = $model->username;
            $user->password_reset_token = Yii::$app->security->generateRandomString();
            $user->generateAuthKey();

            if ($model->password) {
                $user->setPassword($model->password);
            }

            if ($model->status != 10) {
                $user->generateEmailVerificationToken();
            }

            if ($user->save()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'user' => $user
        ]);
    }


    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id): \yii\web\Response
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * @param $id
     * @return User|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id): User
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
