<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?= Yii::$app->homeUrl ?>" class="brand-link">
        <img src="<?= $assetDir ?>/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light"> Lavivion</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?= Yii::$app->homeUrl  ?>images/users/man.jpg" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block"><?= Yii::$app->user->identity->username ?></a>
            </div>
        </div>

        <nav class="mt-2">
            <?php
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => [
                    [
                        'label' => Yii::t('app', 'Users'),
                        'icon' => '',
                        'url' => ['/users'],
                    ],
                    [
                        'label' => Yii::t('app', 'Administration'),
                        'icon' => '',
                        'url' => '',
                        'visible' => Yii::$app->user->can('admin'),
                        'items' => [
                            [
                                'label' => Yii::t('app', 'Routes'),
                                'icon' => '',
                                'url' => ['/admin/route'],
                            ],
                            [
                                'label' => Yii::t('app', 'Permission'),
                                'icon' => '',
                                'url' => ['/admin/permission'],
                            ],
                            [
                                'label' => Yii::t('app', 'Role'),
                                'icon' => '',
                                'url' => ['/admin/role'],
                            ],
                            [
                                'label' => Yii::t('app', 'Assignment'),
                                'icon' => '',
                                'url' => ['/admin/assignment'],
                            ],
                            [
                                'label' => Yii::t('app', 'User'),
                                'icon' => '',
                                'url' => ['/admin/user'],
                            ],
                        ]
                    ],
                    [
                        'label' => Yii::t('app', 'Authors'),
                        'url' => ['/authors'],
                        'icon' => '',
                        'visible' => !Yii::$app->user->isGuest
                    ],
                    [
                        'label' => Yii::t('app', 'Books'),
                        'url' => ['/books'],
                        'icon' => '',
                        'visible' => !Yii::$app->user->isGuest
                    ],
                ],
            ]);
            ?>
        </nav>
    </div>
</aside>
