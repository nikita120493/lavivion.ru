<?php

use common\models\Book;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var backend\models\BookSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Books');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="book-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Book'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'slug',
            [
                'attribute' => 'author_id',
                'filter' => \common\models\Author::list(),
                'value' => function ($model) {
                    return $model->author->name;
                }
            ],
//            'short_description',
            //'description:ntext',
            //'seo_title',
            //'seo_keywords',
            //'seo_description',

            [
                'attribute' => 'created_by',
                'value' => function ($model) {
                    return \common\models\User::findOne($model->updated_by)->username;
                }
            ],
            [
                'attribute' => 'updated_by',
                'value' => function ($model) {
                    return \common\models\User::findOne($model->updated_by)->username;
                }
            ],
            'created_at',
            'updated_at',
            'is_status:boolean',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Book $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
