<?php

namespace frontend\modules\api\models;

use common\models\Book;

class BookApi extends Book
{

    /**
     * @return array
     */
    public function fields()
    {
        return [
            'title' => 'title',
            'slug' => 'slug',
            'short_description' => 'short_description',
            'description' => 'description',
            'seo_title' => 'seo_title',
            'seo_keywords' => 'seo_keywords',
            'seo_description' => 'seo_description',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'is_status' => 'is_status',
            'author_id' => function ($model) {
                return $model->author->name;
            },
            'author' => 'author',
        ];
    }

    /**
     * @return string[]
     */
    public function extraFields()
    {
        return ['author'];
    }

    /**
     * @return \common\models\AuthorsQuery|\yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(AuthorApi::class, ['id' => 'author_id']);
    }
}
