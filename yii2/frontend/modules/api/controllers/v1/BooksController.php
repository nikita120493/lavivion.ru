<?php

namespace frontend\modules\api\controllers\v1;

use frontend\modules\api\models\BookApi;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use Yii;

class BooksController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'list' => ['GET'],
                'by-id' => ['GET'],
                'update' => ['POST'],
                'delete' => ['DELETE'],
            ],
        ];
        return $behaviors;
    }

    /**
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public function actionList(): ActiveDataProvider
    {
        $query = BookApi::find()->active();

        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        $page = $requestParams['page'] ?? 0;
        $pageSize = $requestParams['per-page'] ?? 20;

        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            //'totalCount' => $query->count(),
            'page' => $page ? $page - 1 : 0,
        ]);


        return Yii::createObject([
            'class' => ActiveDataProvider::class,
            'query' => $query,
            'pagination' => $pagination,
            'sort' => [
                'params' => $requestParams,
            ],
        ]);

    }

    /**
     * @param $id
     * @return object
     * @throws \yii\base\InvalidConfigException
     */
    public function actionById($id): ActiveDataProvider
    {
        $query = BookApi::find()->active()->andWhere(['id' => $id]);

        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        $page = $requestParams['page'] ?? 0;
        $pageSize = $requestParams['per-page'] ?? 20;

        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            //'totalCount' => $query->count(),
            'page' => $page ? $page - 1 : 0,
        ]);

        return Yii::createObject([
            'class' => ActiveDataProvider::class,
            'query' => $query,
            'pagination' => $pagination,
            'sort' => [
                'params' => $requestParams,
            ],
        ]);
    }

    /**
     * @param $id
     * @return false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = BookApi::findOne($id);

        if (!empty($model)) {
            return $model->delete();
        }

        return false;
    }
}
