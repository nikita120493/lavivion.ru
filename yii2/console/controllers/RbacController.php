<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    /**
     * @return void
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function actionInit(): void
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $createPost = $auth->createPermission('createUser');
        $createPost->description = 'Create a user';
        $auth->add($createPost);

        $updatePost = $auth->createPermission('updateUser');
        $updatePost->description = 'Update user';
        $auth->add($updatePost);

        $deleteUser = $auth->createPermission('deleteUser');
        $deleteUser->description = 'delete user';
        $auth->add($deleteUser);

        $author = $auth->createRole('manager');
        $auth->add($author);
        $auth->addChild($author, $createPost);


        $admin = $auth->createRole('administration');
        $auth->add($admin);

        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $auth->addChild($admin, $updatePost);
        $auth->addChild($admin, $author);

        $auth->assign($admin, 1);
    }
}
