<?php

namespace frontend\modules\api\models;

use common\models\Author;

class AuthorApi extends Author
{

    /**
     * @return string[]
     */
    public function fields()
    {
        return [
            'name' => 'name',
            'slug' => 'slug',
            'short_description' => 'short_description',
            'description' => 'description',
            'seo_title' => 'seo_title',
            'seo_keywords' => 'seo_keywords',
            'seo_description' => 'seo_description',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
            'is_status' => 'is_status',
        ];
    }
}
