<?php

namespace frontend\controllers;

use common\models\Book;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class AuthorsController extends Controller
{
    public function actionBooks($slug)
    {
        $query =
            Book::find()
                ->leftJoin('authors', 'authors.id=author_id')
                ->andWhere(['books.is_status' => true, 'authors.slug' => $slug]);

        $dataProvider = new ActiveDataProvider(['query' => $query,]);

        return $this->render('books', compact('dataProvider'));
    }
}
